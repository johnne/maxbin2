# README #

Repository for modified [MaxBin2](https://downloads.jbei.org/data/microbial_communities/MaxBin/MaxBin.html).

### What is this repository for? ###

Original MaxBin2 software requires FragGeneScan to predict ORFs on contigs. 
Because FragGeneScan can be difficult to get working (on OSX) or may have conflicting perl dependencies (linux) this repository 
version aims to use prodigal instead for ORF prediction.
